package ru.t1.chernysheva.tm.service.model;

import liquibase.Liquibase;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.api.service.model.IUserService;
import ru.t1.chernysheva.tm.configuration.ServerConfiguration;
import ru.t1.chernysheva.tm.migration.AbstractSchemeTest;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.marker.UnitCategory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static ru.t1.chernysheva.tm.constant.ProjectConstant.*;

@Category(UnitCategory.class)
public class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);

    @NotNull
    private IProjectService projectService = context.getBean(IProjectService.class);

    @NotNull
    private IUserService userService = context.getBean(IUserService.class);

    @NotNull
    private List<Project> projectList;

    @Nullable
    private static User USER_1;

    @Nullable
    private static User USER_2;

    private static long USER_ID_COUNTER = 0;

    @BeforeClass
    public static void changeSchema() throws LiquibaseException {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    @Before
    public void init() {
        USER_ID_COUNTER++;
        USER_1 = userService.create("proj_serv_mod_usr_1_" + USER_ID_COUNTER, "1");
        USER_2 = userService.create("proj_serv_mod_usr_2_" + USER_ID_COUNTER, "1");
        projectList = new ArrayList<>();
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUser(USER_1);
            projectList.add(project);
            projectService.add(USER_1.getId(), project);
        }
        for (int i = 1; i <= INIT_COUNT_PROJECTS; i++) {
            @NotNull final Project project = new Project();
            project.setName("Project_" + i);
            project.setDescription("Description_" + i);
            project.setUser(USER_2);
            projectList.add(project);
            projectService.add(USER_2.getId(), project);
        }
    }

    @After
    public void closeConnection() {
        userService.clear();
    }

    @Test
    public void testClearPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_1.getId()).size());
        projectService.clear(USER_1.getId());
        Assert.assertEquals(0, projectService.findAll(USER_1.getId()).size());
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_2.getId()).size());
        projectService.clear(USER_2.getId());
        Assert.assertEquals(0, projectService.findAll(USER_2.getId()).size());
    }

    @Test
    public void testClearNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.clear(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID));
    }

    @Test
    public void testFindAllPositive() {
        @NotNull List<Project> projects = projectService.findAll(USER_1.getId());
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_2.getId());
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_2.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(UUID.randomUUID().toString());
        Assert.assertEquals(Collections.emptyList(), projects);
    }

    @Test
    public void testFindAllSortNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(NULLABLE_USER_ID, CREATED_ENTITY_SORT));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findAll(EMPTY_USER_ID, CREATED_ENTITY_SORT));
    }

    @Test
    public void testFindAllSortPositive() {
        List<Project> projects = projectService.findAll(USER_1.getId(), CREATED_ENTITY_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_1.getId(), NAME_ENTITY_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_1.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_1.getId(), STATUS_ENTITY_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().equals(USER_1.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
        projects = projectService.findAll(USER_2.getId(), NULLABLE_ENTITY_SORT);
        Assert.assertNotNull(projects);
        for (final Project project : projectList) {
            if (project.getUser().getId().equals(USER_2.getId()))
                Assert.assertNotNull(
                        projects.stream()
                                .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                                .filter(m -> project.getId().equals(m.getId()))
                                .findFirst()
                                .orElse(null)
                );
        }
    }

    @Test
    public void testAddProjectNegative() {
        @NotNull final Project project = new Project();
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(NULLABLE_USER_ID, project));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.add(EMPTY_USER_ID, project));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.add(USER_1.getId(), NULLABLE_PROJECT_MODEL));
    }

    @Test
    public void testAddProjectPositive() {
        @NotNull Project project = new Project();
        project.setName("ProjectAddTest");
        project.setDescription("ProjectAddTest desc");
        project.setUser(USER_1);
        projectService.add(USER_1.getId(), project);
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_1.getId()).size());
        project.setId(UUID.randomUUID().toString());
    }

    @Test
    public void testExistsByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.existsById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_1.getId(), NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.existsById(USER_1.getId(), EMPTY_PROJECT_ID));
    }

    @Test
    public void testExistsByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertTrue(projectService.existsById(project.getUser().getId(), project.getId()));
        }
    }

    @Test
    public void testFindOneByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(NULLABLE_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneById(EMPTY_USER_ID, projectList.get(0).getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_1.getId(), NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.findOneById(USER_1.getId(), EMPTY_PROJECT_ID));
    }

    @Test
    public void testFindOneByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertEquals(project.getId(), projectService.findOneById(project.getUser().getId(), project.getId()).getId());
        }
    }

    @Test
    public void testFindOneByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(NULLABLE_USER_ID, 0));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.findOneByIndex(EMPTY_USER_ID, 0));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.findOneByIndex(USER_1.getId(), NULLABLE_INDEX));
    }

    @Test
    public void testFindOneByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Assert.assertEquals(projectList.get(i).getId(), projectService.findOneByIndex(USER_1.getId(), i + 1).getId());
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Assert.assertEquals(projectList.get(i).getId(), projectService.findOneByIndex(USER_2.getId(), i - 4).getId());
        }
    }

    @Test
    public void testRemoveByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(NULLABLE_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeById(EMPTY_USER_ID, NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_1.getId(), NULLABLE_PROJECT_ID));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.removeById(USER_1.getId(), EMPTY_PROJECT_ID));
    }

    @Test
    public void testRemoveByIdPositive() {
        for (final Project project : projectList) {
            projectService.removeById(project.getUser().getId(), project.getId());
            Assert.assertFalse(projectService.findAll(project.getUser().getId()).contains(project));
        }
        Assert.assertEquals(0, projectService.findAll(USER_1.getId()).size());
        Assert.assertEquals(0, projectService.findAll(USER_2.getId()).size());
    }

    @Test
    public void testRemoveByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(NULLABLE_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.removeByIndex(EMPTY_USER_ID, NULLABLE_INDEX));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.removeByIndex(USER_1.getId(), NULLABLE_INDEX));
    }

    @Test
    public void testCreateProjectNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.create(EMPTY_USER_ID, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_1.getId(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.create(USER_1.getId(), "", null));
    }

    @Test
    public void testCreateProjectPositive() {
        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_1.getId()).size());
        projectService.create(USER_1.getId(), "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_1.getId()).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS, projectService.findAll(USER_2.getId()).size());
        projectService.create(USER_2.getId(), "PROJ", "PROJ_DESC");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_2.getId()).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_1.getId()).size());
        projectService.create(USER_1.getId(), "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_1.getId()).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 1, projectService.findAll(USER_2.getId()).size());
        projectService.create(USER_2.getId(), "PROJ_2", "");
        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_2.getId()).size());

        Assert.assertEquals(INIT_COUNT_PROJECTS + 2, projectService.findAll(USER_1.getId()).size());
        projectService.create(USER_1.getId(), "PROJ_3", null);
        Assert.assertEquals(INIT_COUNT_PROJECTS + 3, projectService.findAll(USER_1.getId()).size());
    }

    @Test
    public void testChangeProjectStatusByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(NULLABLE_USER_ID, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusById(EMPTY_USER_ID, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_1.getId(), null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.changeProjectStatusById(USER_1.getId(), "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusById(USER_1.getId(), UUID.randomUUID().toString(), null));
        Assert.assertThrows(StatusEmptyException.class, () -> projectService.changeProjectStatusById(USER_1.getId(), projectService.findOneByIndex(USER_1.getId(), 1).getId(), null));
    }

    @Test
    public void testChangeProjectStatusByIdPositive() {
        for (final Project project : projectList) {
            Assert.assertNotNull(projectService.changeProjectStatusById(project.getUser().getId(), project.getId(), IN_PROGRESS_STATUS));
            Assert.assertEquals(IN_PROGRESS_STATUS, projectService.findOneById(project.getUser().getId(), project.getId()).getStatus());
        }
    }

    @Test
    public void testChangeProjectStatusByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(NULLABLE_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.changeProjectStatusByIndex(EMPTY_USER_ID, NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_1.getId(), NULLABLE_INDEX, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.changeProjectStatusByIndex(USER_1.getId(), -1, null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.changeProjectStatusByIndex(UUID.randomUUID().toString(), 1, null));
    }

    @Test
    public void testChangeProjectStatusByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            projectService.changeProjectStatusByIndex(USER_1.getId(), 1, IN_PROGRESS_STATUS);
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Project project = projectList.get(i);
            Assert.assertNotNull(projectService.findAll(USER_1.getId()).stream()
                    .filter(m -> project.getId().equals(m.getId()))
                    .filter(m -> IN_PROGRESS_STATUS.equals(m.getStatus()))
                    .findFirst()
                    .orElse(null));
        }
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            projectService.changeProjectStatusByIndex(USER_2.getId(), 1, IN_PROGRESS_STATUS);
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS + 5; i++) {
            Project project = projectList.get(i);
            Assert.assertNotNull(projectService.findAll(USER_2.getId()).stream()
                    .filter(m -> project.getId().equals(m.getId()))
                    .filter(m -> IN_PROGRESS_STATUS.equals(m.getStatus()))
                    .findFirst()
                    .orElse(null));
        }
    }

    @Test
    public void testUpdateByIdNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateById(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_1.getId(), null, null, null));
        Assert.assertThrows(IdEmptyException.class, () -> projectService.updateById(USER_1.getId(), "", null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_1.getId(), UUID.randomUUID().toString(), null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateById(USER_1.getId(), UUID.randomUUID().toString(), "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateById(USER_1.getId(), UUID.randomUUID().toString(), "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIdPositive() {
        for (final Project project : projectList) {
            projectService.updateById(project.getUser().getId(), project.getId(), project.getName(), null);
            projectService.updateById(project.getUser().getId(), project.getId(), project.getName(), "");
            projectService.updateById(project.getUser().getId(), project.getId(), project.getName() + "_upd", project.getDescription() + "_upd");
            Assert.assertEquals(project.getId(), projectService.findOneById(project.getUser().getId(), project.getId()).getId());
            Assert.assertEquals(project.getName() + "_upd", projectService.findOneById(project.getUser().getId(), project.getId()).getName());
            Assert.assertEquals(project.getDescription() + "_upd", projectService.findOneById(project.getUser().getId(), project.getId()).getDescription());
        }
    }

    @Test
    public void testUpdateByIndexNegative() {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(NULLABLE_USER_ID, null, null, null));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectService.updateByIndex(EMPTY_USER_ID, null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_1.getId(), null, null, null));
        Assert.assertThrows(IndexIncorrectException.class, () -> projectService.updateByIndex(USER_1.getId(), -1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_1.getId(), 1, null, null));
        Assert.assertThrows(NameEmptyException.class, () -> projectService.updateByIndex(USER_1.getId(), 1, "", null));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.updateByIndex(UUID.randomUUID().toString(), 1, "UPD_PROJ_1", null));
    }

    @Test
    public void testUpdateByIndexPositive() {
        for (int i = 0; i < INIT_COUNT_PROJECTS; i++) {
            Project project = projectList.get(i);
            projectService.updateByIndex(project.getUser().getId(), i + 1, project.getName(), null);
            projectService.updateByIndex(project.getUser().getId(), i + 1, project.getName(), "");
            projectService.updateByIndex(project.getUser().getId(), i + 1, project.getName() + "_upd", project.getDescription() + "_upd");
            project.setName(project.getName() + "_upd");
            Assert.assertNotNull(
                    projectService.findAll(project.getUser().getId()).stream()
                            .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                            .filter(m -> project.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
        for (int i = 5; i < INIT_COUNT_PROJECTS * 2; i++) {
            Project project = projectList.get(i);
            projectService.updateByIndex(project.getUser().getId(), i - 4, project.getName(), null);
            projectService.updateByIndex(project.getUser().getId(), i - 4, project.getName(), "");
            projectService.updateByIndex(project.getUser().getId(), i - 4, project.getName() + "_upd", project.getDescription() + "_upd");
            project.setName(project.getName() + "_upd");
            Assert.assertNotNull(
                    projectService.findAll(project.getUser().getId()).stream()
                            .filter(m -> project.getUser().getId().equals(m.getUser().getId()))
                            .filter(m -> project.getName().equals(m.getName()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

}
