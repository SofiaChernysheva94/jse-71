package ru.t1.chernysheva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.dto.ISessionDtoService;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;
import ru.t1.chernysheva.tm.exception.field.IdEmptyException;
import ru.t1.chernysheva.tm.exception.field.IndexIncorrectException;
import ru.t1.chernysheva.tm.exception.field.UserIdEmptyException;
import ru.t1.chernysheva.tm.exception.system.AuthenticationException;
import ru.t1.chernysheva.tm.repository.dto.ISessionDtoEntityRepository;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionDtoService implements ISessionDtoService {

    @NotNull
    @Autowired
    private ISessionDtoEntityRepository repository;


    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<SessionDTO> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        final Pageable pageable = PageRequest.of(index - 1, 1);
        @Nullable final List<SessionDTO> sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return null;
        @Nullable SessionDTO result = sessionList.get(0);
        return result;
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.existsByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        final Pageable pageable = PageRequest.of(index - 1, 1);
        @Nullable final List<SessionDTO> sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return;
        @Nullable SessionDTO session = sessionList.get(0);
        repository.deleteAllByUserIdAndId(userId, session.getId());
    }

    @Override
    @Transactional
    public void remove(@NotNull SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserIdAndId(session.getUserId(), session.getId());
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final SessionDTO session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        session.setUserId(userId);
        repository.save(session);
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO add(@NotNull final SessionDTO session) {
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new UserIdEmptyException();
        repository.save(session);
        return session;
    }

}
