package ru.t1.chernysheva.tm.api.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.enumerated.Status;

import java.util.List;

public interface ITaskDtoService {

    void deleteByUserId(@Nullable final String userId);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    boolean existsByIdAndId(@Nullable final String userId, @Nullable final String id);

    @Nullable
    TaskDTO findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    TaskDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<TaskDTO> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    @Transactional
    TaskDTO changeTaskStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    @Transactional
    TaskDTO changeTaskStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void add(@Nullable String userId, @Nullable TaskDTO project);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

    @NotNull
    @SneakyThrows
    @Transactional
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @NotNull
    @SneakyThrows
    @Transactional
    List<TaskDTO> findAllByTaskId(@Nullable String userId, @Nullable String projectId);
}
