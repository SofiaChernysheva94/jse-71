package ru.t1.chernysheva.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDtoEntityRepository extends IDtoUserOwnedRepository<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByUserIdAndProjectId(@NotNull String userId, @NotNull String projectId);

}
