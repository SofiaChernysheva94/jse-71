package ru.t1.chernysheva.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.chernysheva.tm.dto.model.AbstractModelDTO;

@NoRepositoryBean
public interface IDtoRepository<M extends AbstractModelDTO> extends JpaRepository<M, String> {
}
