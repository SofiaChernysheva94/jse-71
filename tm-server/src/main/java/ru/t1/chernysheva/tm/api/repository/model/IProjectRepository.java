package ru.t1.chernysheva.tm.api.repository.model;

import ru.t1.chernysheva.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}
