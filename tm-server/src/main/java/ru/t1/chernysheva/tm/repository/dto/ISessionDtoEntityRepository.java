package ru.t1.chernysheva.tm.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;

@Repository
public interface ISessionDtoEntityRepository extends IDtoUserOwnedRepository<SessionDTO> {
}
