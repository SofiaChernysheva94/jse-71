package ru.t1.chernysheva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.model.ITaskService;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.exception.entity.TaskNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.repository.model.ITaskEntityRepository;
import ru.t1.chernysheva.tm.repository.model.IUserModelEntityRepository;

import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskEntityRepository repository;

    @NotNull
    @Autowired
    private IUserModelEntityRepository userRepository;


    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Task result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Task result;
        @Nullable final List<Task> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) return null;
        result = taskList.get(0);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable Task task;
        @Nullable final List<Task> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        repository.deleteByUserIdAndId(userId, task.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    @SuppressWarnings({"unchecked"})
    public List<Task> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Task> result;
        final Sort sortable = Sort.by(entitySort.getSortField());
        result = repository.findAllSortByUserId(userId, sortable);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Task task;
        task = repository.getOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        if (status == null) throw new StatusEmptyException();
        task.setStatus(status);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeTaskStatusByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        @Nullable final Task task;
        @Nullable final List<Task> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        if (status == null) throw new StatusEmptyException();
        task.setStatus(status);
        repository.save(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @Nullable final List<Task> result;
        result = repository.findAllByUserIdAndProjectId(userId, projectId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task = new Task();
        @Nullable final User user = userRepository.getOne(userId);
        task.setUser(user);
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (task == null) throw new TaskNotFoundException();
        if (task.getUser() == null || task.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task;
        task = repository.getOneByUserIdAndId(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(@Nullable final String userId, @Nullable final Integer index, @Nullable final String name, @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task task;
        @Nullable final List<Task> taskList;
        final Pageable pageable = PageRequest.of(index - 1, 1);
        taskList = repository.getOneByIndexAndUserId(userId, pageable);
        if (taskList == null || taskList.isEmpty()) throw new TaskNotFoundException();
        task = taskList.get(0);
        task.setName(name);
        if (description != null && !description.isEmpty())
            task.setDescription(description);
        repository.save(task);
    }

}
