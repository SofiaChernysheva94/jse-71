package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.dto.IProjectDtoService;
import ru.t1.chernysheva.tm.api.dto.IProjectTaskDtoService;
import ru.t1.chernysheva.tm.api.dto.ITaskDtoService;
import ru.t1.chernysheva.tm.api.dto.IUserDtoService;
import ru.t1.chernysheva.tm.api.model.IProjectService;
import ru.t1.chernysheva.tm.api.model.IProjectTaskService;
import ru.t1.chernysheva.tm.api.model.ITaskService;
import ru.t1.chernysheva.tm.api.model.IUserService;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IProjectDtoService getProjectDtoService();

    @NotNull
    IProjectTaskDtoService getProjectTaskDtoService();

    @NotNull
    ITaskDtoService getTaskDtoService();

    @NotNull
    IUserDtoService getUserDtoService();

    @NotNull
    IProjectService getProjectService();

    @NotNull
    IProjectTaskService getProjectTaskService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IPropertyService getPropertyService();

}
