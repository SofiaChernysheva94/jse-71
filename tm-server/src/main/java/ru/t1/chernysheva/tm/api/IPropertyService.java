package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    String getApplicationLog();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    Integer getSessionTimeout();

}
