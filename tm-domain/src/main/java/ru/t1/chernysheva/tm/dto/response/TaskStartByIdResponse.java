package ru.t1.chernysheva.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;

@NoArgsConstructor
public final class TaskStartByIdResponse extends AbstractTaskResponse {

    public TaskStartByIdResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}
