package ru.t1.chernysheva.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! Task id is empty...");
    }

}
