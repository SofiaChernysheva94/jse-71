package ru.t1.chernysheva.tm.exception.system;

import org.jetbrains.annotations.Nullable;

public class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! Command not supported...");
    }

    public CommandNotSupportedException(@Nullable String argument) {
        super("Error! Command ``" + argument + "`` not supported...");
    }

}
