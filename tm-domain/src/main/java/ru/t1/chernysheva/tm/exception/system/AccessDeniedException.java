package ru.t1.chernysheva.tm.exception.system;

public class AccessDeniedException extends AbstractSystemException {

    public AccessDeniedException() {
        super("Error! Access denied...");
    }

}
