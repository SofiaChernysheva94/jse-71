package ru.t1.chernysheva.tm.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.IUserEndpoint;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import ru.t1.chernysheva.tm.dto.request.*;
import ru.t1.chernysheva.tm.marker.SoapCategory;
import ru.t1.chernysheva.tm.service.PropertyService;

@Category(SoapCategory.class)
public class UserEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());

    @Nullable
    private String usualToken;

    @Nullable
    private String adminToken;

    @Before
    @SneakyThrows
    public void init() {
        usualToken = authEndpoint.login(new UserLoginRequest("user 1", "1")).getToken();
        adminToken = authEndpoint.login(new UserLoginRequest("admin", "admin")).getToken();
    }

    @Test
    public void testLoginNegative() {
        @NotNull UserLoginRequest userLoginRequest = new UserLoginRequest();
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.login(userLoginRequest));
        userLoginRequest.setLogin("user");
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.login(userLoginRequest));
        userLoginRequest.setPassword("1");
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.login(userLoginRequest));
    }

    @Test
    @SneakyThrows
    public void testLoginPositive() {
        @NotNull UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("user 1");
        userLoginRequest.setPassword("1");
        Assert.assertNotNull(authEndpoint.login(userLoginRequest));
    }

    @Test
    public void testLogoutNegative() {
        @NotNull UserLogoutRequest userLogoutRequest = new UserLogoutRequest();
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.logout(userLogoutRequest));
    }

    @Test
    @SneakyThrows
    public void testLogoutPositive() {
        @NotNull UserLogoutRequest userLogoutRequest = new UserLogoutRequest(usualToken);
        Assert.assertNotNull(authEndpoint.logout(userLogoutRequest));
    }

    @Test
    public void testRegistryNegative() {
        @NotNull UserRegistryRequest userRegistryRequest = new UserRegistryRequest();
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.registryUser(userRegistryRequest));
        userRegistryRequest.setLogin("");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.registryUser(userRegistryRequest));
        userRegistryRequest.setLogin("newUser");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.registryUser(userRegistryRequest));
        userRegistryRequest.setPassword("");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.registryUser(userRegistryRequest));
        userRegistryRequest.setPassword("11111");
        userRegistryRequest.setLogin("admin");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.registryUser(userRegistryRequest));
    }

    @Test
    @SneakyThrows
    public void testRegistryAndRemoveUser() {
        @NotNull UserRegistryRequest userRegistryRequest = new UserRegistryRequest();
        userRegistryRequest.setLogin("newUser");
        userRegistryRequest.setPassword("newPassword");
        userRegistryRequest.setEmail("newEmail@test.ru");
        userEndpoint.registryUser(userRegistryRequest);
        @NotNull UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("newUser");
        userLoginRequest.setPassword("newPassword");
        Assert.assertNotNull(authEndpoint.login(userLoginRequest));
        @NotNull UserRemoveRequest userRemoveRequest = new UserRemoveRequest(adminToken);
        userRemoveRequest.setLogin("newUser");
        Assert.assertNotNull(userEndpoint.removeUser(userRemoveRequest));
    }

    @Test
    public void testRemoveUserNegative() {
        @NotNull UserRemoveRequest userRemoveRequest = new UserRemoveRequest();
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.removeUser(userRemoveRequest));
        userRemoveRequest.setToken(usualToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.removeUser(userRemoveRequest));
        userRemoveRequest.setToken(adminToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.removeUser(userRemoveRequest));
    }

    @Test
    @SneakyThrows
    public void testUserLockUnlockPositive() {
        @NotNull UserLockRequest userLockRequest = new UserLockRequest(adminToken);
        userLockRequest.setLogin("user 1");
        Assert.assertNotNull(userEndpoint.lockUser(userLockRequest));
        @NotNull UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("user 1");
        userLoginRequest.setPassword("1");
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.login(userLoginRequest));
        @NotNull UserUnlockRequest userUnlockRequest = new UserUnlockRequest(adminToken);
        userUnlockRequest.setLogin("user 1");
        Assert.assertNotNull(userEndpoint.unlockUser(userUnlockRequest));
        Assert.assertNotNull(authEndpoint.login(userLoginRequest));
    }

    @Test
    public void testUserLockNegative() {
        @NotNull UserLockRequest userLockRequest = new UserLockRequest();
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.lockUser(userLockRequest));
        userLockRequest.setToken(usualToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.lockUser(userLockRequest));
        userLockRequest.setToken(adminToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.lockUser(userLockRequest));
        userLockRequest.setLogin("");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.lockUser(userLockRequest));
    }

    @Test
    public void testUserUnlockNegative() {
        @NotNull UserUnlockRequest userUnlockRequest = new UserUnlockRequest();
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.unlockUser(userUnlockRequest));
        userUnlockRequest.setToken(usualToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.unlockUser(userUnlockRequest));
        userUnlockRequest.setToken(adminToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.unlockUser(userUnlockRequest));
        userUnlockRequest.setLogin("");
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.unlockUser(userUnlockRequest));
    }

    @Test
    public void testUserChangePasswordNegative() {
        @NotNull UserChangePasswordRequest userChangePasswordRequest = new UserChangePasswordRequest();
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.changeUserPassword(userChangePasswordRequest));
        userChangePasswordRequest.setToken(usualToken);
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.changeUserPassword(userChangePasswordRequest));
    }

    @Test
    @SneakyThrows
    public void testUserChangePasswordPositive() {
        @NotNull UserChangePasswordRequest userChangePasswordRequest = new UserChangePasswordRequest(usualToken);
        userChangePasswordRequest.setPassword("newPassword");
        Assert.assertNotNull(userEndpoint.changeUserPassword(userChangePasswordRequest));
        @NotNull UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("user 1");
        userLoginRequest.setPassword("1");
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.login(userLoginRequest));
        userLoginRequest.setPassword("newPassword");
        Assert.assertNotNull(authEndpoint.login(userLoginRequest));
        userChangePasswordRequest.setPassword("1");
        Assert.assertNotNull(userEndpoint.changeUserPassword(userChangePasswordRequest));
        userLoginRequest.setPassword("1");
        Assert.assertNotNull(authEndpoint.login(userLoginRequest));
    }

    @Test
    public void testUserViewProfileNegative() {
        Assert.assertThrows(RuntimeException.class, () -> authEndpoint.profile(new UserProfileRequest()));
    }

    @Test
    public void testUserUpdateProfileNegative() {
        Assert.assertThrows(RuntimeException.class, () -> userEndpoint.updateUserProfile(new UserUpdateProfileRequest()));
    }

    @Test
    @SneakyThrows
    public void testUserUpdateAndViewProfilePositive() {
        UserDTO user = authEndpoint.profile(new UserProfileRequest(usualToken)).getUser();
        Assert.assertNotEquals("Sofia", user.getFirstName());
        Assert.assertNotEquals("chernysheva", user.getLastName());
        Assert.assertNotEquals("Andreevich", user.getMiddleName());
        @NotNull UserUpdateProfileRequest userUpdateProfileRequest = new UserUpdateProfileRequest(usualToken);
        userUpdateProfileRequest.setFirstName("Sofia");
        userUpdateProfileRequest.setLastName("chernysheva");
        userUpdateProfileRequest.setMiddleName("Andreevich");
        Assert.assertNotNull(userEndpoint.updateUserProfile(userUpdateProfileRequest));
        user = authEndpoint.profile(new UserProfileRequest(usualToken)).getUser();
        Assert.assertEquals("Sofia", user.getFirstName());
        Assert.assertEquals("chernysheva", user.getLastName());
        Assert.assertEquals("Andreevich", user.getMiddleName());
        userUpdateProfileRequest.setFirstName(null);
        userUpdateProfileRequest.setLastName(null);
        userUpdateProfileRequest.setMiddleName(null);
        Assert.assertNotNull(userEndpoint.updateUserProfile(userUpdateProfileRequest));
    }

}
