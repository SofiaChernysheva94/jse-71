package ru.t1.chernysheva.tm.api;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationName();

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getApplicationConfig();

    String getApplicationLog();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
