package ru.t1.chernysheva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.request.TaskCompleteByIndexRequest;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIndexListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Complete task by index.";

    @NotNull
    public static final String NAME = "task-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskCompleteByIndexListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber();

        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(getToken());
        request.setIndex(index);

        taskEndpoint.completeTaskByIndex(request);
    }

}
