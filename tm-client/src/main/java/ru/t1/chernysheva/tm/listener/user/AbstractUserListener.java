package ru.t1.chernysheva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.IUserEndpoint;
import ru.t1.chernysheva.tm.listener.AbstractListener;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import ru.t1.chernysheva.tm.exception.entity.UserNotFoundException;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    public IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    public IAuthEndpoint authEndpoint;

    protected void showUser(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

}
