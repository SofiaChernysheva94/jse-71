package ru.t1.chernysheva.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.request.ProjectCompleteByIndexRequest;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.util.TerminalUtil;

@Component
public final class ProjectCompleteByIndexListener extends AbstractProjectListener {

    @NotNull
    public static final String DESCRIPTION = "Complete project by index.";

    @NotNull
    public static final String NAME = "project-complete-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@projectCompleteByIndexListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[COMPLETE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber();

        @NotNull final ProjectCompleteByIndexRequest request = new ProjectCompleteByIndexRequest(getToken());
        request.setIndex(index);

        projectEndpoint.completeProjectByIndex(request);
    }

}
