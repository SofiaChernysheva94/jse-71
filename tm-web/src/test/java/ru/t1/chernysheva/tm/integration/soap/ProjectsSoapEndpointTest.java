package ru.t1.chernysheva.tm.integration.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.IProjectCollectionEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chernysheva.tm.client.AuthSoapEndpointClient;
import ru.t1.chernysheva.tm.client.ProjectCollectionSoapEndpointClient;
import ru.t1.chernysheva.tm.client.ProjectSoapEndpointClient;
import ru.t1.chernysheva.tm.dto.CredentialsDTO;
import ru.t1.chernysheva.tm.dto.ProjectDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class ProjectsSoapEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static IProjectCollectionEndpoint projectCollectionEndpoint;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private static String userId;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @NotNull
    private final ProjectDTO projectFourth = new ProjectDTO("Project Test 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new CredentialsDTO("admin", "admin")).getSuccess());
        projectEndpoint = ProjectSoapEndpointClient.getInstance(URL);
        projectCollectionEndpoint = ProjectCollectionSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider projectBindingProvider = (BindingProvider) projectEndpoint;
        @NotNull final BindingProvider projectCollectionBindingProvider = (BindingProvider) projectCollectionEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        projectBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        projectCollectionBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        projectCollectionEndpoint.saveCollection(Arrays.asList(projectFirst, projectSecond));
    }

    @After
    public void clear() {
        if (projectCollectionEndpoint.findAll() != null)
            projectCollectionEndpoint.deleteCollection(projectCollectionEndpoint.findAll());
    }

    @Test
    public void findAllTest() {
        Assert.assertEquals(2, projectCollectionEndpoint.findAll().size());
        @NotNull List<ProjectDTO> projects = Arrays.asList(projectFirst, projectSecond);
        for (final ProjectDTO project : projects) {
            Assert.assertNotNull(
                    projectCollectionEndpoint
                            .findAll()
                            .stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    public void saveCollectionTest() {
        Assert.assertEquals(2, projectCollectionEndpoint.findAll().size());
        projectCollectionEndpoint.saveCollection(Arrays.asList(projectThird, projectFourth));
        Assert.assertEquals(4, projectCollectionEndpoint.findAll().size());
    }

    @Test
    public void updateCollectionTest() {
        Assert.assertEquals(2, projectCollectionEndpoint.findAll().size());
        Assert.assertNotEquals("New name 1", projectCollectionEndpoint.findAll().get(0).getName());
        Assert.assertNotEquals("New name 2", projectCollectionEndpoint.findAll().get(1).getName());
        projectFirst.setName("New name 1");
        projectSecond.setName("New name 2");
        projectCollectionEndpoint.updateCollection(Arrays.asList(projectFirst, projectSecond));
        Assert.assertEquals(2, projectCollectionEndpoint.findAll().size());
        Assert.assertEquals("New name 1", projectCollectionEndpoint.findAll().get(0).getName());
        Assert.assertEquals("New name 2", projectCollectionEndpoint.findAll().get(1).getName());
    }

    @Test
    public void deleteCollectionTest() {
        Assert.assertNotNull(projectCollectionEndpoint.findAll());
        projectCollectionEndpoint.deleteCollection(Arrays.asList(projectFirst, projectSecond));
        Assert.assertNull(projectCollectionEndpoint.findAll());
    }

}

