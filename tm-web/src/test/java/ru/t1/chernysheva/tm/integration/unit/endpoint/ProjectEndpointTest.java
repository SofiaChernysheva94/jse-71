package ru.t1.chernysheva.tm.integration.unit.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import ru.t1.chernysheva.tm.config.ApplicationConfiguration;
import ru.t1.chernysheva.tm.config.WebApplicationConfiguration;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {WebApplicationConfiguration.class, ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectEndpointTest {

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private MockMvc mockMvc;

    @NotNull
    @Autowired
    private WebApplicationContext wac;

    @NotNull
    private static final String PROJECT_URL = "http://localhost:8080/api/project";

    @NotNull
    private static final String PROJECTS_URL = "http://localhost:8080/api/projects";

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @NotNull
    private final ProjectDTO projectFourth = new ProjectDTO("Project Test 4");

    @Before
    public void initTest() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectFirst.setUserId(UserUtil.getUserId());
        projectSecond.setUserId(UserUtil.getUserId());
        projectThird.setUserId(UserUtil.getUserId());
        projectFourth.setUserId(UserUtil.getUserId());
        projectPost(projectFirst);
        projectPost(projectSecond);
    }

    @After
    public void clear() {
        projectsDelete(projectsGet());
    }

    @SneakyThrows
    private ProjectDTO projectGet(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.readValue(json, ProjectDTO.class);
    }

    @SneakyThrows
    private List<ProjectDTO> projectsGet() {
        @NotNull final String json = mockMvc.perform(MockMvcRequestBuilders.get(PROJECTS_URL)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        if ("".equals(json)) return null;
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return Arrays.asList(objectMapper.readValue(json, ProjectDTO[].class));
    }


    @Test
    @SneakyThrows
    public void projectGetTest() {
        Assert.assertEquals(projectFirst.getName(), projectGet(projectFirst.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void projectsGetTest() {
        Assert.assertEquals(2, projectsGet().size());
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectSecond.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void projectPost(@NotNull final ProjectDTO project) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void projectsPost(@NotNull final List<ProjectDTO> projects) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void projectPostTest() {
        Assert.assertEquals(2, projectsGet().size());
        Assert.assertNull(
                projectsGet().stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectPost(projectThird);
        Assert.assertEquals(3, projectsGet().size());
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void projectsPostTest() {
        Assert.assertEquals(2, projectsGet().size());
        projectsPost(Arrays.asList(projectThird, projectFourth));
        Assert.assertEquals(4, projectsGet().size());
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectFourth.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @SneakyThrows
    private void projectPut(@NotNull final ProjectDTO project) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECT_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void projectsPut(@NotNull final List<ProjectDTO> projects) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.post(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void projectPutTest() {
        Assert.assertNotEquals("New name for project 1", projectGet(projectFirst.getId()).getName());
        Assert.assertNull(
                projectsGet().stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectFirst.setName("New name for project 1");
        projectPut(projectFirst);
        Assert.assertEquals("New name for project 1", projectGet(projectFirst.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void projectsPutTest() {
        Assert.assertNotEquals("New name for project 1", projectGet(projectFirst.getId()).getName());
        Assert.assertNotEquals("New name for project 2", projectGet(projectSecond.getId()).getName());
        projectFirst.setName("New name for project 1");
        projectSecond.setName("New name for project 2");
        projectsPost(Arrays.asList(projectFirst, projectSecond));
        Assert.assertEquals("New name for project 1", projectGet(projectFirst.getId()).getName());
        Assert.assertEquals("New name for project 2", projectGet(projectSecond.getId()).getName());
    }

    @SneakyThrows
    private void projectDelete(@NotNull final String id) {
        @NotNull final String url = PROJECT_URL + "/" + id;
        mockMvc.perform(MockMvcRequestBuilders.delete(url)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @SneakyThrows
    private void projectsDelete(@NotNull final List<ProjectDTO> projects) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        mockMvc.perform(MockMvcRequestBuilders.delete(PROJECTS_URL).content(json)
                        .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void projectDeleteTest() {
        Assert.assertEquals(2, projectsGet().size());
        Assert.assertNotNull(
                projectsGet().stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectDelete(projectFirst.getId());
        Assert.assertEquals(1, projectsGet().size());
        Assert.assertNull(
                projectsGet().stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void projectsDeleteTest() {
        Assert.assertEquals(2, projectsGet().size());
        projectsDelete(Arrays.asList(projectFirst, projectSecond));
        Assert.assertEquals(0, projectsGet().size());
    }

}
