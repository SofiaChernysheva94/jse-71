package ru.t1.chernysheva.tm.integration.unit.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.config.ApplicationConfiguration;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.exception.AuthException;
import ru.t1.chernysheva.tm.exception.IdEmptyException;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.util.UserUtil;

import java.util.Arrays;
import java.util.List;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @NotNull
    private final ProjectDTO projectFourth = new ProjectDTO("Project Test 4");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectFirst.setUserId(UserUtil.getUserId());
        projectSecond.setUserId(UserUtil.getUserId());
        projectThird.setUserId(UserUtil.getUserId());
        projectDTOService.save(UserUtil.getUserId(), projectFirst);
        projectDTOService.save(UserUtil.getUserId(), projectSecond);
    }

    @After
    public void clear() {
        projectDTOService.removeAll(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void saveTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.save(null, projectFirst));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.save(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void saveAllTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.saveAll(null, Arrays.asList(projectFirst)));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.saveAll(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.removeAll(null));
    }

    @Test
    @SneakyThrows
    public void removeCollectionTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.removeAll(null, Arrays.asList(projectFirst)));
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.removeOneById(null, projectFirst.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.removeOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.removeOneById(UserUtil.getUserId(), ""));
    }

    @Test
    @SneakyThrows
    public void removeOneTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.removeOne(null, projectFirst));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.removeOne(UserUtil.getUserId(), null));
    }

    @Test
    @SneakyThrows
    public void findAllTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.findAll(null));
    }

    @Test
    @SneakyThrows
    public void findOneByIdTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.findOneById(null, projectFirst.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.findOneById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.findOneById(UserUtil.getUserId(), ""));
    }

    @Test
    @SneakyThrows
    public void getProjectNameByIdTestNegative() {
        Assert.assertThrows(AuthException.class, () -> projectDTOService.getProjectNameById(null, projectFirst.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.getProjectNameById(UserUtil.getUserId(), null));
        Assert.assertThrows(IdEmptyException.class, () -> projectDTOService.getProjectNameById(UserUtil.getUserId(), ""));
    }

    @Test
    @SneakyThrows
    public void saveTestPositive() {
        Assert.assertEquals(2, projectDTOService.findAll(UserUtil.getUserId()).size());
        projectDTOService.save(UserUtil.getUserId(), projectThird);
        List<ProjectDTO> projects = projectDTOService.findAll(UserUtil.getUserId());
        Assert.assertEquals(3, projectDTOService.findAll(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                projects.stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void saveAllTestPositive() {
        Assert.assertEquals(2, projectDTOService.findAll(UserUtil.getUserId()).size());
        projectDTOService.saveAll(UserUtil.getUserId(), Arrays.asList(projectThird, projectFourth));
        Assert.assertEquals(4, projectDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeAllByUserIdTestPositive() {
        Assert.assertEquals(2, projectDTOService.findAll(UserUtil.getUserId()).size());
        projectDTOService.removeAll(UserUtil.getUserId());
        Assert.assertEquals(0, projectDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeCollectionTestPositive() {
        Assert.assertEquals(2, projectDTOService.findAll(UserUtil.getUserId()).size());
        projectDTOService.removeAll(UserUtil.getUserId(), Arrays.asList(projectFirst, projectSecond));
        Assert.assertEquals(0, projectDTOService.findAll(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void removeOneByIdTestPositive() {
        Assert.assertNotNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectDTOService.removeOneById(UserUtil.getUserId(), projectFirst.getId());
        Assert.assertNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void removeOneTestPositive() {
        Assert.assertNotNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectDTOService.removeOne(UserUtil.getUserId(), projectFirst);
        Assert.assertNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void findAllTestPositive() {
        Assert.assertEquals(2, projectDTOService.findAll(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        Assert.assertNotNull(
                projectDTOService.findAll(UserUtil.getUserId()).stream()
                        .filter(m -> projectSecond.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void findOneByIdTestPositive() {
        Assert.assertEquals(projectFirst.getName(), projectDTOService.findOneById(UserUtil.getUserId(), projectFirst.getId()).getName());
    }

    @Test
    @SneakyThrows
    public void getProjectNameByIdTestPositive() {
        Assert.assertEquals(projectFirst.getName(), projectDTOService.findOneById(UserUtil.getUserId(), projectFirst.getId()).getName());
    }

}

