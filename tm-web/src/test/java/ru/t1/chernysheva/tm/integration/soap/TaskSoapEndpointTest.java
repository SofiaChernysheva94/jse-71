package ru.t1.chernysheva.tm.integration.soap;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.springframework.data.util.CastUtils;
import org.springframework.http.HttpHeaders;
import ru.t1.chernysheva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.ITaskCollectionEndpoint;
import ru.t1.chernysheva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.chernysheva.tm.client.AuthSoapEndpointClient;
import ru.t1.chernysheva.tm.client.TaskCollectionSoapEndpointClient;
import ru.t1.chernysheva.tm.client.TaskSoapEndpointClient;
import ru.t1.chernysheva.tm.dto.CredentialsDTO;
import ru.t1.chernysheva.tm.dto.TaskDTO;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.*;

public class TaskSoapEndpointTest {

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskCollectionEndpoint taskCollectionEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @NotNull
    private static String userId;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final TaskDTO taskFirst = new TaskDTO("Task Test 1");

    @NotNull
    private final TaskDTO taskSecond = new TaskDTO("Task Test 2");

    @NotNull
    private final TaskDTO taskThird = new TaskDTO("Task Test 3");

    @NotNull
    private final TaskDTO taskFourth = new TaskDTO("Task Test 4");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login(new CredentialsDTO("admin", "admin")).getSuccess());
        taskEndpoint = TaskSoapEndpointClient.getInstance(URL);
        taskCollectionEndpoint = TaskCollectionSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider taskBindingProvider = (BindingProvider) taskEndpoint;
        @NotNull final BindingProvider taskCollectionBindingProvider = (BindingProvider) taskCollectionEndpoint;
        Map<String, List<String>> headers = CastUtils.cast((Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS));
        if (headers == null) new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        taskBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
        taskCollectionBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @Before
    public void init() {
        taskCollectionEndpoint.saveCollection(Arrays.asList(taskFirst, taskSecond));
    }

    @After
    public void clear() {
        if (taskCollectionEndpoint.findAll() != null)
            taskCollectionEndpoint.deleteCollection(taskCollectionEndpoint.findAll());
    }

    @Test
    public void findOneByIdTest() {
        Assert.assertEquals(taskFirst.getName(), taskEndpoint.findOneById(taskFirst.getId()).getName());
    }

    @Test
    public void saveOneTest() {
        Assert.assertEquals(2, taskCollectionEndpoint.findAll().size());
        taskEndpoint.saveOne(taskThird);
        Assert.assertEquals(3, taskCollectionEndpoint.findAll().size());
    }

    @Test
    public void updateOneTest() {
        Assert.assertNotEquals("New name 1", taskEndpoint.findOneById(taskFirst.getId()).getName());
        taskFirst.setName("New name 1");
        taskEndpoint.updateOne(taskFirst);
        Assert.assertEquals("New name 1", taskEndpoint.findOneById(taskFirst.getId()).getName());
    }

    @Test
    public void deleteOneByIdTest() {
        Assert.assertEquals(2, taskCollectionEndpoint.findAll().size());
        taskEndpoint.deleteOneById(taskFirst.getId());
        Assert.assertEquals(1, taskCollectionEndpoint.findAll().size());
    }

}
