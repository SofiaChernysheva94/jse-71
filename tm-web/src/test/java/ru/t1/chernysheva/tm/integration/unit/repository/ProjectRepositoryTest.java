package ru.t1.chernysheva.tm.integration.unit.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.config.ApplicationConfiguration;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.marker.UnitCategory;
import ru.t1.chernysheva.tm.repository.dto.IProjectDTORepository;
import ru.t1.chernysheva.tm.util.UserUtil;

import java.util.List;

@Transactional
@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {ApplicationConfiguration.class})
@Category(UnitCategory.class)
public class ProjectRepositoryTest {

    @NotNull
    @Autowired
    private IProjectDTORepository projectDTORepository;

    @NotNull
    @Autowired
    private AuthenticationManager authenticationManager;

    @NotNull
    private final ProjectDTO projectFirst = new ProjectDTO("Project Test 1");

    @NotNull
    private final ProjectDTO projectSecond = new ProjectDTO("Project Test 2");

    @NotNull
    private final ProjectDTO projectThird = new ProjectDTO("Project Test 3");

    @Before
    public void initTest() {
        @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("admin", "admin");
        @NotNull final Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        projectFirst.setUserId(UserUtil.getUserId());
        projectSecond.setUserId(UserUtil.getUserId());
        projectThird.setUserId(UserUtil.getUserId());
        projectDTORepository.save(projectFirst);
        projectDTORepository.save(projectSecond);
    }

    @After
    public void clear() {
        projectDTORepository.deleteAllByUserId(UserUtil.getUserId());
    }

    @Test
    @SneakyThrows
    public void findAllByUserIdTest() {
        List<ProjectDTO> projects = projectDTORepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(2, projects.size());
        for (final ProjectDTO project : projects) {
            Assert.assertNotNull(
                    projects.stream()
                            .filter(m -> project.getId().equals(m.getId()))
                            .findFirst()
                            .orElse(null)
            );
        }
    }

    @Test
    @SneakyThrows
    public void findByIdAndUserIdTest() {
        Assert.assertEquals(projectFirst.getId(), projectDTORepository.findByIdAndUserId(projectFirst.getId(), UserUtil.getUserId()).getId());
    }

    @Test
    @SneakyThrows
    public void saveOneTest() {
        Assert.assertEquals(2, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        projectDTORepository.save(projectThird);
        List<ProjectDTO> projects = projectDTORepository.findAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(3, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                projects.stream()
                        .filter(m -> projectThird.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

    @Test
    @SneakyThrows
    public void deleteAllByUserIdTest() {
        Assert.assertEquals(2, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        projectDTORepository.deleteAllByUserId(UserUtil.getUserId());
        Assert.assertEquals(0, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
    }

    @Test
    @SneakyThrows
    public void deleteByIdAndUserIdTest() {
        Assert.assertEquals(2, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNotNull(
                projectDTORepository.findAllByUserId(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
        projectDTORepository.deleteByIdAndUserId(projectFirst.getId(), UserUtil.getUserId());
        Assert.assertEquals(1, projectDTORepository.findAllByUserId(UserUtil.getUserId()).size());
        Assert.assertNull(
                projectDTORepository.findAllByUserId(UserUtil.getUserId()).stream()
                        .filter(m -> projectFirst.getId().equals(m.getId()))
                        .findFirst()
                        .orElse(null)
        );
    }

}
