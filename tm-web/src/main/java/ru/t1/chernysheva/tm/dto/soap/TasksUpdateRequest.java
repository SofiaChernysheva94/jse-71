package ru.t1.chernysheva.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.TaskDTO;

import javax.xml.bind.annotation.*;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "tasksUpdateRequest")
public class TasksUpdateRequest {

    @XmlElement(required = true)
    protected List<TaskDTO> tasks;

}
