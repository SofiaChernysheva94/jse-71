package ru.t1.chernysheva.tm.model;

import lombok.Getter;
import lombok.Setter;
import ru.t1.chernysheva.tm.enumerated.RoleType;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "app_role")
@Getter
@Setter
public class Role {

    @Id
    private String id = UUID.randomUUID().toString();

    @ManyToOne
    @JoinColumn(nullable = false, name = "user_id")
    private User user;

    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.USER;

    @Override
    public String toString() {
        return roleType.name();
    }

}
