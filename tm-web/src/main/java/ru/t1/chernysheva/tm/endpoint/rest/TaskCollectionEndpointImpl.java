package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.endpoint.ITaskCollectionEndpoint;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.TaskDTO;
import ru.t1.chernysheva.tm.model.CustomUser;
import ru.t1.chernysheva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/tasks")
@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.ITaskCollectionEndpoint")
public class TaskCollectionEndpointImpl implements ITaskCollectionEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Override
    @Nullable
    @GetMapping
    @WebMethod
    public List<TaskDTO> findAll() {
        return taskService.findAll(UserUtil.getUserId());
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.saveAll(UserUtil.getUserId(), tasks);
    }

    @Override
    @DeleteMapping
    @WebMethod
    public void deleteCollection(
            @WebParam(name = "tasks", partName = "tasks")
            @NotNull @RequestBody List<TaskDTO> tasks) {
        taskService.removeAll(UserUtil.getUserId(), tasks);
    }

}

