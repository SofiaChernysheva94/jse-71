package ru.t1.chernysheva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.http.auth.AuthenticationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.exception.AuthException;
import ru.t1.chernysheva.tm.exception.IdEmptyException;
import ru.t1.chernysheva.tm.exception.NameEmptyException;
import ru.t1.chernysheva.tm.repository.dto.IProjectDTORepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ProjectDTOService implements IProjectDTOService {

    @NotNull
    @Autowired
    private IProjectDTORepository repository;

    @Override
    @Transactional
    @SneakyThrows
    public void save(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null) throw new AuthException();
        if (project == null) throw new IdEmptyException();
        if (project.getName() == null || project.getName().isEmpty()) throw new NameEmptyException();
        project.setUserId(userId);
        repository.save(project);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void saveAll(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects) {
        if (userId == null) throw new AuthException();
        if (projects == null) throw new IdEmptyException();
        if (projects.isEmpty()) throw new IdEmptyException();
        for (@NotNull ProjectDTO projectDTO : projects) {
            projectDTO.setUserId(userId);
            if (projectDTO.getName() == null || projectDTO.getName().isEmpty()) throw new NameEmptyException();
        }
        repository.saveAll(projects);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId, @Nullable final Collection<ProjectDTO> projects) {
        if (userId == null) throw new AuthenticationException();
        repository.deleteAll(projects);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOne(@Nullable final String userId, @Nullable final ProjectDTO project) {
        if (userId == null) throw new AuthException();
        if (project == null) throw new IdEmptyException();
        repository.deleteByIdAndUserId(project.getId(), userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public String getProjectNameById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId).getName();
    }

}
