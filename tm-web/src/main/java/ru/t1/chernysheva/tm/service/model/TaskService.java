package ru.t1.chernysheva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.apache.http.auth.AuthenticationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.model.ITaskService;
import ru.t1.chernysheva.tm.exception.AuthException;
import ru.t1.chernysheva.tm.exception.IdEmptyException;
import ru.t1.chernysheva.tm.exception.NameEmptyException;
import ru.t1.chernysheva.tm.model.Task;
import ru.t1.chernysheva.tm.model.User;
import ru.t1.chernysheva.tm.repository.model.ITaskRepository;
import ru.t1.chernysheva.tm.repository.model.IUserRepository;

import java.util.Collection;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @Override
    @Transactional
    @SneakyThrows
    public void save(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null) throw new AuthException();
        if (task == null) throw new IdEmptyException();
        if (task.getName() == null || task.getName().isEmpty()) throw new NameEmptyException();
        @Nullable final User user = userRepository.findById(userId).get();
        task.setUser(user);
        repository.save(task);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void saveAll(@Nullable final String userId, @Nullable final Collection<Task> tasks) {
        if (userId == null) throw new AuthException();
        if (tasks == null) throw new IdEmptyException();
        if (tasks.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.findById(userId).get();
        for (@NotNull Task taskDTO : tasks) {
            taskDTO.setUser(user);
            if (taskDTO.getName() == null || taskDTO.getName().isEmpty()) throw new NameEmptyException();
        }
        repository.saveAll(tasks);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        repository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeAll(@Nullable final String userId, @Nullable final Collection<Task> tasks) {
        if (userId == null) throw new AuthException();
        repository.deleteAll(tasks);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByIdAndUserId(id, userId);
    }

    @Override
    @Transactional
    @SneakyThrows
    public void removeOne(@Nullable final String userId, @Nullable final Task task) {
        if (userId == null) throw new AuthException();
        if (task == null) throw new IdEmptyException();
        repository.deleteByIdAndUserId(task.getId(), userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Task> findAll(@Nullable final String userId) {
        if (userId == null) throw new AuthException();
        return repository.findAllByUserId(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public Task findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null) throw new AuthException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findByIdAndUserId(id, userId);
    }

}
