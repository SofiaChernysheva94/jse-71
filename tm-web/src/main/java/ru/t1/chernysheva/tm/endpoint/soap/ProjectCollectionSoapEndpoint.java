package ru.t1.chernysheva.tm.endpoint.soap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.dto.soap.*;
import ru.t1.chernysheva.tm.util.UserUtil;

@Endpoint
public class ProjectCollectionSoapEndpoint {

    @NotNull
    public final static String LOCATION_URI = "/ws";

    @NotNull
    public final static String PORT_TYPE_NAME = "ProjectCollectionSoapEndpointPort";

    @NotNull
    public final static String NAMESPACE = "http://tm.chernysheva.t1.ru/dto/soap";


    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Nullable
    @ResponsePayload
    @PayloadRoot(localPart = "projectsFindAllRequest", namespace = NAMESPACE)
    public ProjectsFindAllResponse findCollection(
            @RequestPayload final ProjectsFindAllRequest request) {
        return new ProjectsFindAllResponse(projectDTOService.findAll(UserUtil.getUserId()));
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsSaveRequest", namespace = NAMESPACE)
    public ProjectsSaveResponse saveCollection(
            @RequestPayload final ProjectsSaveRequest request) {
        projectDTOService.saveAll(UserUtil.getUserId(), request.getProjects());
        return new ProjectsSaveResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsUpdateRequest", namespace = NAMESPACE)
    public ProjectsUpdateResponse updateCollection(
            @RequestPayload final ProjectsUpdateRequest request) {
        projectDTOService.saveAll(UserUtil.getUserId(), request.getProjects());
        return new ProjectsUpdateResponse();
    }

    @ResponsePayload
    @PayloadRoot(localPart = "projectsDeleteRequest", namespace = NAMESPACE)
    public ProjectsDeleteResponse deleteCollection(
            @RequestPayload final ProjectsDeleteRequest request) {
        projectDTOService.removeAll(UserUtil.getUserId(), request.getProjects());
        return new ProjectsDeleteResponse();
    }

}
