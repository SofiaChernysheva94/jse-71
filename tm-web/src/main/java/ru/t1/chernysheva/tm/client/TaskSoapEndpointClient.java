package ru.t1.chernysheva.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.api.endpoint.ITaskEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.URL;

public class TaskSoapEndpointClient {

    @SneakyThrows
    public static ITaskEndpoint getInstance(@NotNull final String baseURL) {
        @NotNull final String wsdl = baseURL + "/jaxws/TaskEndpointWS?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "TaskEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.chernysheva.t1.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final ITaskEndpoint result = Service.create(url, name).getPort(ITaskEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
