package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.TaskDTO;
import ru.t1.chernysheva.tm.model.CustomUser;
import ru.t1.chernysheva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/task")
@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.ITaskEndpoint")
public class TaskEndpointImpl implements ITaskEndpoint {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @Override
    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    public TaskDTO findOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        return taskService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateOne(
            @WebParam(name = "task", partName = "task")
            @NotNull @RequestBody TaskDTO task) {
        taskService.save(UserUtil.getUserId(), task);
    }

    @Override
    @DeleteMapping("/{id}")
    @WebMethod
    public void deleteOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        taskService.removeOneById(UserUtil.getUserId(), id);
    }

}
