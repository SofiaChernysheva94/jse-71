package ru.t1.chernysheva.tm.dto.soap;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.chernysheva.tm.dto.ProjectDTO;

import javax.xml.bind.annotation.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@XmlType(name = "")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "projectSaveRequest")
public class ProjectSaveRequest {

    @XmlElement(required = true)
    protected ProjectDTO project;

}
