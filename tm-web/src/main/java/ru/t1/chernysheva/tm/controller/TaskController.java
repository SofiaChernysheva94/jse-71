package ru.t1.chernysheva.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.dto.ITaskDTOService;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.dto.TaskDTO;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.CustomUser;

import java.util.List;

@Controller
public class TaskController {

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/task/create")
    public String create(@AuthenticationPrincipal final CustomUser user) {
        @NotNull TaskDTO task = new TaskDTO("New Task" + System.currentTimeMillis());
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/task/delete/{id}")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        taskService.removeOneById(user.getUserId(), id);
        return "redirect:/tasks";
    }

    @PostMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("task") TaskDTO task,
            BindingResult result
    ) {
        if (task.getProjectId().isEmpty()) task.setProjectId(null);
        taskService.save(user.getUserId(), task);
        return "redirect:/tasks";
    }

    @GetMapping("/task/edit/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id) {
        final TaskDTO task = taskService.findOneById(user.getUserId(), id);
        final List<ProjectDTO> projects = projectService.findAll(user.getUserId());
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("task-edit");
        modelAndView.addObject("task", task);
        modelAndView.addObject("projects", projects);
        modelAndView.addObject("statuses", Status.values());
        return modelAndView;
    }

}
