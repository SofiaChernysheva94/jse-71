package ru.t1.chernysheva.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.dto.ProjectDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IProjectEndpoint {

    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    ProjectDTO findOneById(String id);

    @PostMapping
    @WebMethod
    void saveOne(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project);

    @PutMapping
    @WebMethod
    void updateOne(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project);

    @DeleteMapping("/{id}")
    @WebMethod
    void deleteOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id);

}
