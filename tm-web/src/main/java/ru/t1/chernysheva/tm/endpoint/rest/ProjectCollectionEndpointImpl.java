package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.endpoint.IProjectCollectionEndpoint;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.model.CustomUser;
import ru.t1.chernysheva.tm.model.Project;
import ru.t1.chernysheva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@RestController
@RequestMapping("/api/projects")
@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.IProjectCollectionEndpoint")
public class ProjectCollectionEndpointImpl implements IProjectCollectionEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @GetMapping
    @WebMethod
    public List<ProjectDTO> findAll() {
        return projectDTOService.findAll(UserUtil.getUserId());
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.saveAll(UserUtil.getUserId(), projects);
    }

    @Override
    @DeleteMapping
    @WebMethod
    public void deleteCollection(
            @WebParam(name = "projects", partName = "projects")
            @NotNull @RequestBody List<ProjectDTO> projects) {
        projectDTOService.removeAll(UserUtil.getUserId(), projects);
    }

}


