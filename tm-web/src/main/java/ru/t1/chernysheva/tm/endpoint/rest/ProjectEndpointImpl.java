package ru.t1.chernysheva.tm.endpoint.rest;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.chernysheva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.chernysheva.tm.api.service.dto.IProjectDTOService;
import ru.t1.chernysheva.tm.api.service.model.IProjectService;
import ru.t1.chernysheva.tm.dto.ProjectDTO;
import ru.t1.chernysheva.tm.util.UserUtil;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@RestController
@RequestMapping("/api/project")
@WebService(endpointInterface = "ru.t1.chernysheva.tm.api.endpoint.IProjectEndpoint")
public class ProjectEndpointImpl implements IProjectEndpoint {

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @Nullable
    @GetMapping("/{id}")
    @WebMethod
    public ProjectDTO findOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        return projectDTOService.findOneById(UserUtil.getUserId(), id);
    }

    @Override
    @PostMapping
    @WebMethod
    public void saveOne(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(UserUtil.getUserId(), project);
    }

    @Override
    @PutMapping
    @WebMethod
    public void updateOne(
            @WebParam(name = "project", partName = "project")
            @NotNull @RequestBody ProjectDTO project) {
        projectDTOService.save(UserUtil.getUserId(), project);
    }

    @Override
    @DeleteMapping("/{id}")
    @WebMethod
    public void deleteOneById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable("id") String id) {
        projectService.removeOneById(UserUtil.getUserId(), id);
    }

}
