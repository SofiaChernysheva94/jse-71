<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>T1 TASK MANAGER</title>
</head>
<style>
    h1 {
        font-size: 1.6 emp;
    }

    a {
        color: darkblue;
    }

    select {
        width: 200px;
    }

    input[type="text"] {
        width: 200px;
    }

    input[type="date"] {
        width: 200px;
    }

</style>
<body>
<table width="100%" height="100%" border="1" style="">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">
            <b>TASK MANAGER</b>
        </td>
        <td width="100%" align="right">
            <a href="/projects">PROJECTS</a>
            |
            <a href="/tasks"> TASKS</a>
            <sec:authorize access="!isAuthenticated()">
                |
                <a href="/login">LOGIN</a>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                |
                USER: <sec:authentication property="name"/>
            </sec:authorize>
            <sec:authorize access="isAuthenticated()">
                |
                <a href="/logout">LOGOUT</a>
            </sec:authorize>
        </td>
    </tr>
    <tr>
        <td colspan="2" height="100%" valign="top" style="">
